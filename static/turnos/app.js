'use strict';

var today = new Date();

$( document ).on( 'ready', function () {
	$.ajax({
		type: 'GET',
		url: 'https://script.google.com/macros/s/AKfycbwMSHIdkbGFAEcML3s-LmKnNSLyV1PNMQd7IUOJtp5EE9tUREI/exec',
		success: function ( data ) {

			$( "#urlBtn" ).attr( 'href', data[0].url ).show();

			loadTabs ( data );

			$( "#tabs" ).tabs();
		},
		error: function ( jqXHR, textStatus, errorThrown ) {
			alert( errorThrown );
		},
	});
});

function loadTabs ( data ) {

	var tabsArray = Array();

	for ( var i in data ) {
		tabsArray.push( data[i].sheetName );
	};

	var tabs = eliminateDuplicates ( tabsArray );

	for ( var t in tabs ) {

		$( "#myUl" ).append( 
			"<li><a href='#tab-" + t + "'>" + tabs[t] + "</a></li>"
		);

		$( "#myDiv" ).append( 
			"<div class='tabla' id='tab-" + t + "'>" + 
				"<h3 style='margin-top: 10px;'>" + tabs[t] + "</h3>" + 
				"<table class='table table-hover table-striped table-bordered'>" +
					"<thead>" +
						"<th>Nombre</th>" +
						"<th>Turno Hoy</th>" +
						"<th>Turno Mañana</th>" +
						"<th>Turno Pasado Mañana</th>" +
					"</thead>" +
					"<tbody></tbody>" +
				"</table>" +
			"</div>" 
		);

		for ( var d in data ) {

			if ( tabs[t] === data[d].sheetName ) {

				for ( var s in data[d].shifts ) {

					if ( today.getDate() === data[d].shifts[s].num ) {

						var table = $( "#tab-" + t ).find('table').find('tbody');

						var myRow = "<tr><td>" + data[d].name + "</td><td>" + 
									data[d].shifts[s].days + " - " + data[d].shifts[s].value + 
									"</td><td>";

						if ( data[d].shifts[parseInt( s ) + 1] ) {

							myRow += data[d].shifts[parseInt( s ) + 1].days + " - " + 
									data[d].shifts[parseInt( s ) + 1].value + "</td><td>";

						} else {

							myRow += " ND </td><td>";

						};
						
						if ( data[d].shifts[parseInt( s ) + 2] ) {

							myRow += data[d].shifts[parseInt( s ) + 2].days + " - " + 
									data[d].shifts[parseInt( s ) + 2].value + "</td></tr>";

						} else {

							myRow += " ND </td></tr>";

						};

						table.append( myRow );

					};
				};

			};
		};

	};
};

function eliminateDuplicates ( arr ) {
	var i;
	var len = arr.length;
	var out = [];
	var obj = {};
	for ( i = 0; i < len; i++ ) {		
		obj[arr[i]] = 0;
	};
	for ( var i in obj ) {
		out.push(i);
	};
	return out;
};