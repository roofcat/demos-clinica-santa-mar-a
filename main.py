# encoding:utf-8
#!/usr/bin/env python


import os
import webapp2
import jinja2


JINJA_ENVIRONMENT = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.abspath('templates')),
	autoescape=True,
	extensions=['jinja2.ext.autoescape']
)


class MainHandler(webapp2.RequestHandler):
    def get(self):
    	template = JINJA_ENVIRONMENT.get_template('/turnos/index.html')
        self.response.write(template.render())

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=False)
